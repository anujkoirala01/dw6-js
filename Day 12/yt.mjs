// Default value

// Array Destructuring and Default values

// let [a = "a", b = "b", c = 4] = [1, 2];

// console.log(a);
// console.log(b);
// console.log(c);

// Object Destructuring and Default Values

// let { name = "Mern", age = 120, weight = 99 } = { name: "mern", weight: 100 };

// console.log(name);
// console.log(age);
// console.log(weight);

// Spread Operator --> It opens wrapper

// let arr1 = [1, 2, 3, ...["a", "b"]];
// console.log(arr1);

// let arr2 = [1, ...["a", "b"], 2, 3, ...["mern", "stack"], "deerwalk"];
// console.log(arr2);

// let ar1 = [1, 2];
// let ar2 = [3, 4];
// let ar3 = [0, ...ar1, ...ar2];      // Concatenate array
// console.log(ar3);

// Let vS Const

// const age = 120;
// age = 140; Cannot be re-initialized as 'const' is used to initialize the variable
// let name = "mern";
// name = "Mern Stack"; // Can be re-initialized

// Block Level Scope

{
  //   console.log(x);   // Cannot access 'x' before initialization --> Error
  //   let x = 1;
  //   console.log(x); // Console is done inside the scope of x --> No Error
}
// console.log(x); // The scope of x is only inside {} --> Error

// Searches in own block and then in parent block

// Rest Operator

// let fun = (a, b, ...c) => {
//   console.log(a);
//   console.log(b);
//   console.log(c);
// };

// fun(1, 2, 3, 4, 5);

// let [a, b, ...c] = [1, 2, 3, 4, 5];
// console.log(a);
// console.log(b);
// console.log(c);

// let { name, ...info } = { name: "mern", age: 120, isHard: false };

// console.log(name);
// console.log(info);
