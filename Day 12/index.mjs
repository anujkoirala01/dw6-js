// Input and output both are array and the length is also same so --> using map method

// let products = [
//   {
//     id: 1,
//     title: "Product 1",
//     category: "electronics",
//     price: 5000,
//     description: "This is description and Product 1",
//     discount: {
//       type: "other",
//     },
//   },
//   {
//     id: 2,
//     title: "Product 2",
//     category: "cloths",
//     price: 2000,
//     description: "This is description and Product 2",
//     discount: {
//       type: "other",
//     },
//   },
//   {
//     id: 3,
//     title: "Product 3",
//     category: "electronics",
//     price: 3000,
//     description: "This is description and Product 3",
//     discount: {
//       type: "other",
//     },
//   },
// ];

// Find the array of id i.e output must be [1,2,3]

// let output = products.map((value, i) => {
//   return value.id;
// });

// console.log(output);

// Find the array of title

// let output = products.map((value, i) => {
//   return value.title;
// });

// console.log(output);

// Find the array of category

// let output = products.map((value, i) => {
//   return value.category;
// });

// console.log(output);

// Find the array of type

// let output = products.map((value, i) => {
//   return value.discount.type;
// });

// console.log(output);

// Find the array of price where each price is multiplied by 3, output must be [15000,6000,9000]

// let output = products.map((value, i) => {
//   return value.price * 3;
// });

// console.log(output);

// Find those array of title whose price is >= 3000

// let inputFilter = products.filter((value, i) => {
//   if (value.price >= 3000) {
//     return true;
//   }
// });
// let output = inputFilter.map((value, i) => {
//   return value.title;
// });

// console.log(output);

// Find those array of title whose price does not equal to 5000

// let inputFilter = products.filter((value, i) => {
//   if (value.price !== 5000) {
//     return true;
//   }
// });

// let output = inputFilter.map((value, i) => {
//   return value.title;
// });

// console.log(output);

// Find those array of category whose price equal to 3000

// let inputFilter = products.filter((value, i) => {
//   if (value.price === 3000) {
//     return true;
//   }
// });

// let output = inputFilter.map((value, i) => {
//   return value.category;
// });

// console.log(output);

// How to generate error

// let error = new Error("error 404 not found");
// throw error;

// Try and Catch [Error Handling]

// try {
//   console.log("I am  try block.");
//   let error = new Error("This is error.");
//   throw error;
// } catch (error) {
//   console.log("I am catch block.");
//   console.log(error.message); // To display error message
// }

// Destructuring Array/ Object

// let list = [1, 2, 3, 4]; // Array

// let [a, b, c, d] = [1, 2, 3, 4]; // Array Destructuring

// console.log(a);
// console.log(b);
// console.log(c);
// console.log(d);
// console.log([a, b, c, d]);

// Object Destructuring

// let { age, name, isHard } = { name: "Mern", age: 120 }; // Order does not matter
// console.log(name);
// console.log(age);
// console.log(isHard);
// console.log({ age, name, isHard });

let { age, name, info } = {
  name: "mern",
  age: 120,
  info: { address: "Sifal" },
};
console.log(age);
console.log(name);
console.log(info);

let { address } = info;

console.log(address);
