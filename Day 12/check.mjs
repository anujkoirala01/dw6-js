let logic = (my_input) => {
  const alphabets = "abcdefghijklmnopqrstuvwxyz";
  let outputString = "";
  let count = 0;

  for (let i = 0; i < my_input.length; i++) {
    const eachChar = my_input[i].toLowerCase();

    if (alphabets.includes(eachChar)) {
      const index = alphabets.indexOf(eachChar);

      const replacedWith = index % 2 === 0 ? "0" : "1";

      outputString += replacedWith;
      count++;
    } else {
      outputString += my_input[i];
    }

    if (count % 5 === 0) {
      outputString += "";
    }
  }
  return outputString.trim();
};

let my_input = console.log(logic(my_input));
