//Function

//define function
//call function
//pass value and get value
//it is not necessary to have equal number of argument and parameters
//default parameter
//return

//define function
// let sum = function () {
//   console.log("I am sum");
// };

// let add = function () {
//   console.log("I am add");
// };

//call
// sum();

// add();

// let sum = function (a = 10, b = 9, c = 5) {
//   console.log(a);
//   console.log(b);
//   console.log(c);
// };

// sum("ram", 2);

// let sum = function (num1, num2) {
//   let _sum = num1 + num2;
//   return _sum;
// };
// let __sum = sum(1, 2);
// console.log(__sum);

// let mul = function (num1, num2) {
//   let _mul = num1 * num2;
//   console.log("a");
//   return _mul;
//   console.log("last"); //always put return at last
// };

// let __mul = mul(5, 3);
// console.log(__mul);

//undefined --> variable that is defined but not initialized

// let sum = function (a, b) {
//   console.log(a);
//   console.log(b);
// };

// sum(1);

// let sum = function (a) {
//   console.log(a);
// if there is no return by default it returns to the call section with return value undefined
// };

// console.log("first");

// let _sum = sum(1);

// console.log(_sum);

// console.log("ok");

//how to check type of data

// let a = 1;
// let b = "name";
// let c = false;
// let d;
// console.log(typeof a);
// console.log(typeof b);
// console.log(typeof c);
// console.log(typeof d);

// Comparator
// ===
// /!==
// >, <
// >=, <=

// let a = 1 === 1;
// console.log(a);

//arithmetic operator     // ctrl + . --> correct words

// let a = 0;
// a++;
// console.log(a);

// let a = 10 % 3;
// console.log(a);
