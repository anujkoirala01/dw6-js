// "nitan"  => "Nitan"
//        split("")                      map()                       join("")
//"nitan"======> ["n","i","t","a","n"]========>["N","i","t","a","n"] =>=>=>=>"Nitan"
// let firstLetterCapital = (name) => {
//name = "Nitan"
//   let nameArr = name.split(""); //["n","i","t","a","n"]
//   let newNameArr = nameArr.map((value, i) => {
//["N","i","t","a","n"]
//     if (i === 0) {
//       return value.toUpperCase();
//     } else {
//       return value.toLowerCase();
//     }
//   });
//   let output = newNameArr.join(""); //"Nitan"
//   return output;
// };
// let name = firstLetterCapital("nitan");
// console.log(name);

// "my name is nitan"=======>["my","name","is","nitan"] =======>["My","Name","Is","Nitan"]   ======>"My Name Is Nitan"
// let eachWordCapital = (sentence) => {
//sentence = "my name"
//   let sentenceArr = sentence.split(" "); //["my","name"]
//   let newSentenceArr = sentenceArr.map((value, i) => {
//     let capitalizeWord = firstLetterCapital(value); //"My"
//     return capitalizeWord;
//   });
//   let output = newSentenceArr.join(" ");
//   return output;
// };
// let _eachWordCapital = eachWordCapital("my name is nitan");
// console.log(_eachWordCapital);

// Filter

// [1,2,3] --> [1]

// let input = ["a", "b", "c"];

// let output = input.filter((value, i) => {
//   if (value === "c") {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(output);

// filter greater than 17

// let input = [1, 18, 2, 29, 30];

// let output = input.filter((value, i) => {
//   if (value > 17) {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(output);

// filter even [1,2,6,3]

// let input = [1, 2, 6, 3];

// let output = input.filter((value, i) => {
//   if (value % 2 === 0) {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(output);
