// a

export let func = () => {
  let ar1 = [1, 3, 4, 5];
  let ar2 = ar1.map((value, i) => {
    if (value % 2 !== 0) {
      return 3 * value;
    } else {
      return 2 * value;
    }
  });
  console.log(ar2);
};

// b

export let doubleArr = (arr) => {
  let arr1 = arr.map((value, i) => {
    return 2 * value;
  });
  console.log(arr1);
};

// c

export let oddByZeroEvenBy2 = (arr) => {
  let arr1 = arr.map((value, i) => {
    if (i % 2 === 0) {
      return 2 * value;
    } else {
      return 0;
    }
  });
  console.log(arr1);
};

// d
