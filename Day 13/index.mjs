// Primitive and Non primitive

// Memory allocation for primitive
// In primitive, for every 'let' a new memory space is created

// let a =1
// let b=1
// let c=b // Let--> creates memory space right that moment

// Memory allocation for non primitive
// If there is 'let' a new memory space is created but first it sees whether the variable is copy of another
// variable, if the variable is copy of another variable , it at that time will not create memory for
// that variable rather it shares memory

// let ar1=[1,2]
// let ar2=[1,2]
// let ar3=ar2  // First checks whether there is copy or not, if there is --> ar2 shares the memory space with ar3 --> ar2 and ar3 same

// For primitive --> === value should be same to return true

// For non primitive --> === value and memory should be same to return true

// let ar1 = [1, 2];
// let ar2 = [1, 2];
// let ar3 = ar1;

// console.log(ar1 == ar2); //false
// console.log(ar1 == ar3); //true

// while push

// let ar4 = [1, 2];
// let ar5 = ar4;
// ar4.push(3);
// console.log(ar4);
// console.log(ar5); // same output as ar4 and ar5 shares same memory

// Default value

// For function
// let fun1 = (a, b, c = 4) => {//receiver
//   console.log(a);
//   console.log(b);
//   console.log(c);
// };
// fun1(1, 2);//giver

// For array
// let [a, b, c = 10, d = 9] = [1, 2, 3];
// console.log(a);
// console.log(b);
// console.log(c);
// console.log(d);

// For object
// let {
//   name = "nitan",
//   age = 18,
//   isMarried = false,
// } = { name: "sandeep", age: 35 };
// console.log(name);
// console.log(age);
// console.log(isMarried);

// Alias in Object

// let { name: realName, age } = { name: "mern", age: 120 }; // realName is alias of name to avoid conflict
// let { name, area } = { name: "Nepal", area: 147181 };
// console.log(name);
// console.log(realName);

// let { name: realName, age = 18 } = { name: "mern", age: 29 }; // Always place default value at last
// let { name: countryName = "india", area } = { name: "Nepal", area: 147181 };

// console.log(realName);
// console.log(countryName);

// Spread Operator --> while creating new array or new object

// Array
// let ar1 = [1, 2, 3];
// let ar2 = [4, 5, 6];

// let ar3 = ["a", "b", ...ar1, "c", ...ar2];
// console.log(ar3);

// Object

// let info = { name: "mern", age: 120 };
// let info1 = { isHard: false, name: "Mern Stack" };
// let info2 = { ...info, ...info1 };
// console.log(info2);

// Rest Operator --> is always used at receiver side

// Function Destructuring
// let fun = (a, b, ...c) => {
//   console.log(a);
//   console.log(b);
//   console.log(c);
// };
// fun(1, 2, 3, 4, 5);

// Array Destructuring

// let [a, ...c] = [1, 2, 3, 4, 5, 6];
// console.log(a);
// console.log(c);

// Object Destructuring

// let { name, ...info } = { name: "mern", age: 120, isHard: false };
// console.log(name); // mern
// console.log(info); // { age: 120, isHard: false }
