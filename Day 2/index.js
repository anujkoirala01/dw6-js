// console.log("My name is AK.");
// console.log(`My name is AK.`);      // ` or ' or "" to wrap the string; ` : backtick
// console.log('My name is AK.');
// console.log(1);
// console.log(true);

// console.log(1+2);
// console.log(1*2);
// console.log(1-2);
// console.log(1/2);

// console.log('a'+'b');       //concatenation: output is string

console.log(2+5+'1'+1+3+5);       //concatenation: output is string ; operation between two at a time only

// Variable in JS

let age =23;            // to define variable --> 'let' keyword

//console.log(age);

// let num1=2;
// let num1=2;      // cannot redefine variable



