// Reduce Method Questions

let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "other",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "other",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "other",
    },
  },
];

// Find the total price, total price is sum of each price of obj element

// let output = products.reduce((pre, cur) => {
//   return pre + cur.price;
// }, 0);
// console.log(output);

// JSON Stringify Parse

// let ar1 = [1, 2, 3];
// let ar1String = JSON.stringify(ar1); // wrapped by ' or "; output --> "[1,2,3]"
// console.log(ar1String); // we do not see ' in the output
// console.log(JSON.parse(ar1String)); // [1,2,3]

// Scoping
// 1. A variable will be known inside its block only
// 2. Inside the block, it is known from the line where it is defined
// 3. A parent variable can  be accessed by child but vice versa is not possible
// 4. A block cannot have same variable twice, but same variable can be defined in different(child) blocks
// 5. If any variable is called in a block, first it searches in its own block if it doesn't find that variable then
//      it will search in parent block and so on
// 6. If any variable is changed in a block, it first searches the variable in its own block i) if the variable exists
//      it changes the value of the variable
//      ii) if it does not find the variable in its own block, it searches the variable in parent blocks, if it finds that variable
//          then it changes the value of the variable in the parent block

// 1.
// 2.
// {
//   console.log(a);      // Generates error since 'a' is not defined above this line
//   let a = 10;
//   console.log(a);
// }
// console.log(a);          // Generates error since 'a' is not defined outside

//3.
// {
//   let a = 3;

//   {
//     let b = 9;
//     console.log(b);
//   }
//   console.log(b); // Generates error since 'b' is out of its scope
// }

// 4.
// {
//   let a = 0;

//   {
//     let a = 1;
//   }
// }

// 5.
// {
//   let a = 3;
//   {
//     let b = 9;
//     console.log(a);
//   }
// }

// {
//   let a = 3;
//   {
//     a = 10; // Does not find the variable in its block, so changes the value of the variable in parent block
//     console.log(a);
//   }
//   console.log(a);
// }

// {
//   let a = 3;
//   {
//     let a = 7;
//     console.log(a);
//   }
//   {
//     console.log(a);
//     a = 9;
//     console.log(a);
//   }
//   console.log(a);
// }

// Higher Order Function and Callback Function
// Higher order function --> if a function takes one or more argument as function then it is called higher order function
// Callback function --> if a function is used as an argument then it is callback function

let fun1 = () => {
  console.log("I am function1");
};

let fun2 = (a, b, c) => {
  console.log("I am function2");
  c();
};

fun2(1, 2, fun1); // fun2 --> Higher order function, fun1 --> callback function
