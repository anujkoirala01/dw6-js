// Find
// Output : One of the element of input or undefined

// let ar1 = [1, 2, 3];

// let ar2 = ar1.find((value, i) => {
//   if (i === 2) {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2);    // output --> 3

// let ar1 = [1, 2, 3];

// let ar2 = ar1.find((value, i) => {
//   if (i === 5) {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2); // output -->undefined

// Some --> output: true or false
// T --> if one of the input return true

// let ar1 = ["a1", "a2", "a3"];

// let ar2 = ar1.some((value, i) => {
//   if (value === "a3") {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2);

// Every --> output: true or false
// T --> if all the input return true

// let ar1 = ["a1", "a2", "a10"];

// let ar2 = ar1.every((value, i) => {
//   if (value === "a1" || value === "a2" || value === "a3") {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2);

// check weather we have all  ages greater than 17 from the given input [1,2,20,30,40]

// let ar1 = [1, 2, 20, 30, 40];

// let ar2 = ar1.every((value, i) => {
//   if (value > 17) {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2);

// check we have nitan in the list ["utshab","nitan","ram","hari"]

// let ar1 = ["utshab", "nitan", "ram", "hari"];

// let ar2 = ar1.some((value, i) => {
//   if (value === "nitan") {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2);

// check weather we have all even in the list [2,4,9,6]

// let ar1 = [2, 4, 6, 9];

// let ar2 = ar1.every((value, i) => {
//   if (value % 2 === 0) {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2);

// check weather all students gets pass mark from the list [ 40,30,80] here pass marks is 40

// let ar1 = [40, 30, 80];

// let ar2 = ar1.every((value, i) => {
//   if (value >= 40) {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(ar2);

// using some method find weather we have Bearer in the the string "Bearer token"

// let input = "Bearer Token";

// let inputArr = input.split(" ");

// let _inputArr = inputArr.some((value, i) => {
//   if (value === "Bearer") {
//     return true;
//   } else {
//     return false;
//   }
// });

// console.log(_inputArr);

// includes --> output : True or False

// let input = ["nitan", "hari", "shyam"];

// let output = input.includes("hari");
// console.log(output);

// let output1 = input.includes("Hari");
// console.log(output1);
