import { checkAdminInRole, checkBearer, checkHabit } from "./assignment.mjs";

// a

// let habits = ["drinking", "smoking", "biting nails"];
// checkHabit(habits);

// b

// let input = "Bearer Token";
// checkBearer(input);

// c

let input = ["admin", "superAdmin", "customer"];
checkAdminInRole(input);
