// Array Length
// let ar1 = ["a", "b", "c"];
// console.log(ar1.length);

// Split method
// let fullName = "my name is mern stack";
// let aArr = fullName.split(" "); //split with space
// console.log(aArr);
// let des = "ramayan";
// let desAns = des.split("a"); //split with a
// console.log(desAns);

// Join method

// let arr1 = ["a", "b", "c"];
// let str1 = arr1.join("*"); // Join with * string --> a*b*c
// let str2 = arr1.join(""); // Join with empty string --> concatenates every array element
// console.log(str1);
// console.log(str2);

// let nameArr = ["My", "name", "is"];
// let arrJoin = nameArr.join(" ");
// console.log(arrJoin);

// Map Method

// let ar1 = ["a", "b", "c"];

// let ar2 = ar1.map((value, i) => {
// // map --> arrow function
//   return `${value}${i}`; // Must have return in order to change the value of the original array
// });

// console.log(ar2);

// let ar1 = [1, 2, 3];

// let ar2 = ar1.map((value, i) => {
//   return value * 3;
// });

// console.log(ar2);

// let ar1 = [1, 2, 3];

// let ar2 = ar1.map((value, i) => {
//   return value + 10;
// });
// console.log(ar2);

// let ar1 = ["my", "name", "is"];

// let ar2 = ar1.map((value, i) => {
//   return `${value.toUpperCase()}N`;
// });
// console.log(ar2);

let name = "my name is mern stack";
let ar = name.split(" ");

let ar1 = ar.map((value, i) => {
  return `${value.toUpperCase()}`;
});

let _name = ar1.join(" ");
console.log(_name);
