// String Method

// let name = "   String Method   ";

// console.log(name.toUpperCase());
// console.log(name.toLowerCase());
// console.log(name.trimStart());
// console.log(name.trim());

// let name = "String Method";

// console.log(name.startsWith("S"));
// console.log(name.startsWith("Sa"));
// console.log(name.endsWith("od"));
// console.log(name.endsWith("ad"));
// console.log(name.startsWith("s"));

// console.log(name.includes("g M"));

// let name = "String Method";

// console.log(name.replaceAll("t", "h"));

// let name = " String Method ";
// console.log(name.length);

// make a arrow function that takes input and return true if it Contain admin or superAdmin

// let isAdminSuperAdmin = (_userInput) => {
//   if (_userInput.includes("admin") || _userInput.includes("superAdmin")) {
//     return true;
//   } else {
//     return false;
//   }
// };

// let _isAdminSuperAdmin = isAdminSuperAdmin("This is admin ");
// console.log(_isAdminSuperAdmin);

// Array Method

let info = ["Array", 6, true];

// console.log(info);
// console.log(info[0]);
// console.log(info[1]);
// console.log(info[2]);

info[1] = 7;
// console.log(info);

// Push
// Pop
// Unshift
// Shift

info.push("Push");
info.push("Pop");
info.pop();
info.unshift("Day");
info.unshift("6");
info.shift();

console.log(info);
