//a
export let sum = (num1, num2) => {
  let num3 = num1 + num2;
  return num3;
};

// b

export let isNitan = (_name) => {
  if (_name === `nitan`) {
    return true;
  } else {
    return false;
  }
};

// c

export let ageCheck = (age) => {
  if (age >= 1 && age <= 12) {
    return `Since Your Age is ${age} your ticket is free`;
  } else if (age >= 13 && age <= 60) {
    return `Since Your Age is ${age} your ticket cost is 80`;
  } else if (age >= 61 && age <= 80) {
    return `Since Your Age is ${age} your ticket cost is 100`;
  } else {
    return `since`;
  }
};

// d

export let percentCheck = (percent) => {
  if (percent >= 90) {
    return 'Grade A';
  } else if (percent >= 80) {
    return `Grade B`;
  } else if (percent >= 70) {
    return `Grade C`;
  } else if (percent >= 60) {
    return `Grade D`;
  } else if (percent >= 40) {
    return `Grade E`;
  } else {
    return `Grade F`;
  }
};
