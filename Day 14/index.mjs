// setTimeOut

// it means after x seconds the callback is pushed to the browser memory queue

// setTimeout(() => {
//   console.log("I will run after 5sec");
// }, 5000); // 5000ms --> 5 sec

// setInterval

// setInterval(() => {
//   console.log("I will run after 2 sec continuously");
// }, 2000);

// console.log("a");

// setTimeout(() => {
//   console.log("cat");
// },0);

// console.log("b");
// Output --> a,b,cat

// 2 sec

// console.log("a");

// setTimeout(() => {
//   console.log("cat");
// }, 2000);
// console.log("b");

// Asynchronous function--> the function that push back to node

// eventLoop
// it constantly monitor JS(call Stack) and memory queue
// if JS(call stack) is available
// it pushes the task from memory queue to JS(call stack --> every line of codes goes to call stack and executes)

// Import and Export
