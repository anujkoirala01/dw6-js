// Truthy and Falsy

import { isGreaterThan18 } from "./assignment1.mjs";

// if ("name") {
//   console.log("Hello");
// }

// if ("") {
//   console.log("True");
// } else {
//   console.log("Falsy part");
// }

// if (1) {
//   console.log("1 Truthy");
// }

// if (0) {
//   console.log("true");
// } else {
//   console.log("0 Falsy");
// }

// let a;
// if (a) {                 // a or undefined
//   console.log("true");
// } else {
//   console.log("Undefined Falsy");
// }

//sum function, S-->N

// let sum = function (num1, num2) {
//   let _sum = Number(num1) + Number(num2);
//   return _sum;
// };

// let __sum = sum("1", "2");
// console.log(__sum);

//For N-->S, String(1)--> "1"

//multiply

// let mul = function (num1, num2) {
//   let _mul = num1 * num2;
//   return _mul;
// };

// let __mul = mul(1, 2);
// console.log(__mul);

// Arrow Function

// let sum = () => {
//   console.log("This is an example of arrow function.");
// };

// sum();

let _isGreaterThan = isGreaterThan18(80);
console.log(_isGreaterThan);
