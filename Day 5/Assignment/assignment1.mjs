// a
export let is18 = (age) => {
  if (age === 18) {
    return true;
  } else {
    return false;
  }
};

// b
export let isGreaterThan18 = (age) => {
  if (age >= 18) {
    return true;
  } else {
    return false;
  }
};

// c
export let isLessThan18 = (age) => {
  if (age <= 18) {
    console.log("You can enter the room");
  } else {
    console.log("You cannot enter the room");
  }
};

// d
export let isEven = (num) => {
  if (num % 2 == 0) {
    return true;
  } else {
    return false;
  }
};

// e

export let average = (num1, num2, num3) => {
  let avg = (num1 + num2 + num3) / 3;
  return avg;
};

// f

export let numCategory = (num) => {
  if (num >= 1 && num <= 10) {
    return "Category 1";
  } else if (num >= 11 && num <= 20) {
    return "Category 2";
  } else if (num >= 21 && num <= 30) {
    return "Category 3";
  }
};

//g

export let ticketCost = (num) => {
  if (num >= 1 && num <= 17) {
    return "Your ticket is free.";
  } else if (num >= 18 && num <= 25) {
    return "Your ticket cost is 100.";
  } else if (num == 26) {
    return "Your ticket cost is 150.";
  } else if (num > 26) {
    return "Your ticket cost is 200.";
  }
};

//h

export let takeNum = (num) => {
  if (num >= 3) {
    console.log("I am greater or equal to 3");
  } else if (num === 3) {
    console.log("I am 3");
  } else if (num < 3) {
    console.log("I am less than 3");
  } else {
    console.log("I am other");
  }
};

export let watchMovie = (age) => {
  if (age >= 18) {
    console.log("You can watch movies");
  } else {
    console.log("You are not authorized to watch movies.");
  }
};
