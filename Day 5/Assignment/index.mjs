import {
  average,
  is18,
  isEven,
  isGreaterThan18,
  isLessThan18,
  numCategory,
  takeNum,
  ticketCost,
  watchMovie,
} from "./assignment1.mjs";

//a

// let _is18 = is18(18);
// console.log(_is18);

//b

// let _isGreaterThan = isGreaterThan18(80);
// console.log(_isGreaterThan);

//c

// isLessThan18(19);

//d

// let _isEven = isEven(15);
// console.log(_isEven);

//e

// let _avg = average(10, 4, 10);
// console.log(_avg);

//f

// let _category = numCategory(25);
// console.log(_category);

//g

// let _ticketCost = ticketCost(27);
// console.log(_ticketCost);

//h

// takeNum(5);

//i

// watchMovie(17);
