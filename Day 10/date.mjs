// Date Object

// Javascript counts months from 0 to 11
// i.e January = 0 and December = 11

// let d = new Date(2023, 15, 24, 10, 33, 30); // add overflow to next year
// console.log(d.toLocaleTimeString());

// let d = new Date(2023, 6, 2, 7, 28, 35); // 6 --> July
// console.log(d);

// One and two digit years will be interpreted as 19xx:

// let d = new Date(99, 9, 5);
// console.log(d);

// let d = new Date(0);
// console.log(d);      // By default 1970/1/1

// JavaScript stores dates as number of milliseconds since January 01, 1970.

// let d = new Date(86400000);
// console.log(d);

// let msec = Date.parse("July 03, 2023");
// console.log(msec);

// Date Get methods

// let d = new Date();
// console.log(d.getFullYear());
// console.log(d.getTime()); // Returns value in milliseconds form 1970/1/1
// console.log(d.getMonth());

// to find the name of the month create an array having all months

// let d = new Date();
// let months = [
//   "January",
//   "February",
//   "March",
//   "April",
//   "May",
//   "June",
//   "July",
//   "August",
//   "September",
//   "October",
//   "November",
//   "December",
// ];

// let month = months[d.getMonth()];
// console.log(month);

// Date Set Methods

let d = new Date();
console.log(d.setFullYear(2024, 11, 3));
