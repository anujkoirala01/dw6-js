// Reverse method in array --> It returns new array as well as changes original array

// let input = [1, 2, 3];

// let output = input.reverse();

// console.log(output);
// console.log(input);

// Reverse the string nitan to natin

// let name = "nitan";
// let nameArr = name.split("");
// let nameReverseArr = nameArr.reverse();
// let nameReverse = nameReverseArr.join("");
// console.log(nameReverse);

// Sort --> It returns new array as well as changes original array

// let input = ["c", "a", "b"];

// let output = input.sort();
// console.log(output);

// Reserve Sort --> In JavaScript we don't have descending sort

// let desOutput = output.reverse(); // Descending using ascending output and reverse
// console.log(desOutput);

// Capital letters are placed first in ascending sort
// ["c","C","b","Z"] --> ["C","Z","b","c"]

// Ascending Sorts
// ["cc","cb"] --> ["cb","cc"]
// ["az","aa"] --> ["aa","az"]
// [10,9] --> [10,9]

// Date

// let todayDate = new Date();
// console.log(todayDate);      // iso format
// console.log(todayDate.toLocaleString());
// console.log(todayDate.toDateString());
// console.log(todayDate.toTimeString());

// console.log(todayDate.toUTCString());
// console.log(todayDate.toString());

// Object Basic
// Object is combination of key value pair

// let info = {
//   name: "mern", // (key: value) --> property
//   weight: 120,
//   isHard: false,
//   age: 10,
//   name: "MERN Stack", // same key then overwrites the previous defined key
// };

// console.log(info);
// console.log(info.name);
// console.log(info.weight);
// console.log(info.isHard);
// console.log(info.age);
// info.age=11
// console.log((info.weight = 140));
// console.log(info);
// delete info.age; // deletes element
// console.log(info);

// Duplicate key does not exist

// console.log(info);

// classwork

// a)let word = "my name is mern stack";
// reverse whole word
//output must be "ym eman si nrem kcats"

// let input = "my name is mern stack";
// let inputArr = input.split(" "); // ['my','name','is','mern','stack']
// let outputArr = inputArr.map((value, i) => {
//   let _input = value.split(""); // my --> ['m','y']
//   let inputRev = _input.reverse(); // ['m','y'] --> ['y','m']
//   let output = inputRev.join(""); // ['y','m'] --> ym
//   return output;
// });
// let revSentence = outputArr.join(" "); // ['ym', 'eman','si','nrem','kcats'] --> "ym eman si nrem kcats"
// console.log(revSentence);
