// Add specific property in object

// let info = {
//   name: "mern",
// };

// console.log(info);
// info.name = "MERN Stack";
// info.age = 120;
// console.log(info);

// Order does not matter

// let info = {
//   name: "Mern",
//   age: 120,
// };
// console.log(info.age);
// let info1 = {
//   age: 120,
//   name: "Mern",
// };
// console.log(info1.age);

// Object to Array Conversion, Entries

// let info = {
//   name: "mern",
//   age: 120,
//   isHard: false,
// };

// ['name',age,isHard] --> array of keys
// ['mern',120,false] --> array of values
// [['name','mern'], ['age',120],['isHard',false]] --> Entries (array of property)

// let keysArr = Object.keys(info);
// console.log(keysArr);

// let valuesArr = Object.values(info);
// console.log(valuesArr);

// let entriesArr = Object.entries(info);   // Array of array
// console.log(entriesArr);

// Array to Object Conversion

// let info = [
//   ["name", "mern"],
//   ["age", 120],
//   ["isHard", false],
// ];

// let obj = Object.fromEntries(info);
// console.log(obj);

// make a function and pass an object, return empty and non empty object

// let func = (info) => {
//   let entriesArr = Object.entries(info);
//   if (entriesArr.length) {
//     return "The object is not empty.";
//   } else {
//     return "The object is empty.";
//   }
// };

// let info = {
//   name: "mern",
//   age: 100,
// };

// let output = func(info);
// console.log(output);

//

// let isObjectEmpty = (obj) => {
//   let entriesArr = Object.entries(obj);
//   if (entriesArr.length) {
//     return false;
//   } else {
//     return true;
//   }
// };

// let _isObjectEmpty = isObjectEmpty({ name: "mern" });
// console.log(_isObjectEmpty);

// Reduce Method --> Array Method

// Find the sum of all elements of an array using reduce method

// let ar1 = [1, 2, 3, 4];

// let arrSum = ar1.reduce((pre, cur) => {
//   return pre + cur;
// }, 0);

// console.log(arrSum);

// Find the product of all elements of array

// let ar1 = [2, 3, 2, 4];

// let product = ar1.reduce((pre, cur) => {
//   return pre * cur;
// }, 1);
// console.log(product);

// Null vs Undefined
// Use null to make the variable empty

let a;
a = 10;
console.log(a);
a = null;
console.log(a);
