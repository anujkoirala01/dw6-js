// 1.Rule for variable

// import { name, surname } from "./index1.mjs";

// let num1 = 5; //Use descriptive name for variable //Use camelCase for variable name
// let num2 = 5;
// let sum = num1 + num2;
// console.log(sum);

//do not redeclare variable, if needed use _

// let name = "Anuj";
// let surname = "Koirala";
// let fullName = name + " " + surname;
// console.log(fullName);

// can use _ and $ only for variable

// 2.Export and Import

// let fullName = name + " " + surname;
// console.log(fullName);

// let name = "Anuj";
// let surname = "Koirala";
// let fullName = `My name is ${name} ${surname}.`; // ${} can be used only in backticks(``)
// console.log(fullName);

// let isMarried = false;

// if (isMarried) {
//   console.log("I am married");
// } else {
//   console.log("I am bachelor");
// }

// let num1 = 1;

// if (num1 < 0) {
//   console.log("Negative number");
// } else if (num1 > 0) {
//   console.log("Positive number");
// } else {
//   console.log("Zero");
// }

let age = 90;
//if else
//it executes from top to bottom and if the condition meets the execution stops right there
if (age >= 1 && age <= 12) {
  console.log("A");
} else if (age === 5) {
  // ===
  console.log("Hello");
} else if (age >= 13 && age <= 20) {
  console.log("B");
} else if (age >= 21 && age <= 30) {
  console.log("C");
} else if (age >= 31 && age <= 40) {
  console.log("D");
} else {
  console.log("E");
}
